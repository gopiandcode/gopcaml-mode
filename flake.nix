{
  description = "Flake providing emacs gopcaml mode";

  inputs = {
    nixpkgs.url = github:nixos/nixpkgs/nixos-22.11;
    flake-utils.url = github:numtide/flake-utils;
    nix-filter.url = github:numtide/nix-filter;
  };

  outputs = { self, nixpkgs, flake-utils, nix-filter }:
    flake-utils.lib.eachDefaultSystem (system: let
      overlay =
        (final: prev: {
          emacsPackagesFor = emacs: (
            (prev.emacsPackagesFor emacs).overrideScope' (
              efinal: _: {
                gopcaml-mode = (final.pkgs.callPackage (import ./default.nix)) {
                  inherit nix-filter;
                  inherit (efinal) trivialBuild ocp-indent tuareg merlin multiple-cursors smartparens;
                };
              })
          );
        });
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ overlay ];
      };
    in {
      overlays = {
        default = overlay;
      };
      devShell = with pkgs;
        mkShell {
          buildInputs = [ (emacsWithPackages (epkgs: with epkgs; [ gopcaml-mode smartparens ] )) ];
        };
    });
}
