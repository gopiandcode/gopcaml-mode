{ ocaml
, ocamlPackages
, trivialBuild
, ocp-indent
, emacs
, tuareg
, merlin
, smartparens
, multiple-cursors
, lib
, nix-filter }:
let
  duneProjectContents = builtins.readFile ./dune-project;
  version = builtins.head (builtins.match "\.*\\(version ([[:digit:]\.]+)\\)\.*" duneProjectContents) + ".git";
  pname = "gopcaml-mode";
  ocamlPackage = ocamlPackages.buildDunePackage rec {
    inherit version pname;
    duneVersion = "3";
    src = nix-filter.lib {
      root = ./.;
      include =
        [ "parser" ] ++
        (map nix-filter.lib.matchName
          [ "dune" "dune-project"])
        ++ (map nix-filter.lib.matchExt [ "ml" "el" "mli" "opam"]);
    };
    buildInputs = with ocamlPackages; [
      ppx_let ppx_deriving ppx_sexp_conv ecaml core
    ];
    postInstall = ''
      path="$out/lib/ocaml/${ocaml.version}/site-lib/${pname}/gopcaml.so"
      sed -i "s/\(gopcaml-library-path\) nil/\1\ \"''${path//\//\\/}\"/g" $out/share/emacs/site-lisp/gopcaml-mode.el
    '';
  };
in (trivialBuild {
  inherit version pname;
  src = ocamlPackage;
  unpackPhase = ''
    cp $src/share/emacs/site-lisp/*.el .
  '';
  # optional dependencies
  buildInputs = [ smartparens multiple-cursors ];
  packageRequires = [ ocp-indent tuareg merlin ];
  meta = with lib; {
    license = licenses.gpl3;
    inherit (emacs.meta) platforms;
  };  
})
